const express = require("express");
const app = express();
const port = 3000;
const dotenv = require("dotenv");
dotenv.config();
const { matchRouter } = require("./src/routers/matchRouter");
const { mongoConnect } = require("./src/utils/mongoConnect");

const { DB_USER, DB_PWD, DB_URL, DB_NAME } = process.env;

const uri = `mongodb+srv://${DB_USER}:${DB_PWD}@${DB_URL}?retryWrites=true&w=majority`;

mongoConnect(DB_NAME, uri);

app.use(express.json());

app.use(matchRouter);

app.get("/", (req, res) =>
  res.status(200).json({
    success: true,
    message: "Health is Good!!",
  })
);

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
