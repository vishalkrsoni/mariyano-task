const { HttpStatusCode } = require("axios");
const playersList = require("../../data/players.json");
const APIResponse = require("../utils/APIResponse");
const { validateAddTeamBody } = require("../validators/validateAddTeamBody");
const {
  isValidPlayer,
  getPlayerTypeByPlayerName,
  getPlayerTeamByPlayerName,
} = require("../utils/filterPlayers");

const validateAddTeam = (req, res, next) => {
  const { error, value } = validateAddTeamBody.validate(req.body);

  if (error) {
    const errorMessage = error.details.map((err) => err.message);
    return res
      .status(HttpStatusCode.BadRequest)
      .send(APIResponse.badRequest("Input Validation failed", errorMessage));
  }

  const { players, captain, viceCaptain } = value;

  // Checking if captain and vice-captain are among the selected players
  if (!players.includes(captain) || !players.includes(viceCaptain)) {
    return res
      .status(HttpStatusCode.BadRequest)
      .send(
        APIResponse.badRequest(
          "Input Validation failed",
          "Captain and vice-captain must be among the selected players."
        )
      );
  }

  const invalidPlayerNames = [];
  const teamInfoMap = new Map();

  players.forEach((player) => {
    if (!isValidPlayer(playersList, player)) {
      invalidPlayerNames.push(player);
    }

    const playerType = getPlayerTypeByPlayerName(playersList, player);
    const playerTeam = getPlayerTeamByPlayerName(playersList, player);

    teamInfoMap.set(playerType, (teamInfoMap.get(playerType) || 0) + 1);
    teamInfoMap.set(playerTeam, (teamInfoMap.get(playerTeam) || 0) + 1);
  });

  console.log(teamInfoMap);

  if (invalidPlayerNames.length > 0)
    return res
      .status(HttpStatusCode.BadRequest)
      .send(
        APIResponse.badRequest(
          "Input Validation failed. Please add valid players",
          invalidPlayerNames
        )
      );

  // Checking if there is min 1 and max 8 players of each type
  for (const [key, value] of teamInfoMap.entries()) {
    if (key !== "Chennai Super Kings" && key !== "Rajasthan Royals") {
      if (value < 1 || value > 8) {
        return res
          .status(HttpStatusCode.BadRequest)
          .send(
            APIResponse.badRequest(
              `Input Validation failed. Min: 1, Max: 8 ${key.toLowerCase()} allowed `,
              `${value} ${key.toLowerCase()} not allowed. `
            )
          );
      }
    } else {
      // checking team constraint
      if (value < 1 || value > 10) {
        return res
          .status(HttpStatusCode.BadRequest)
          .send(
            APIResponse.badRequest(
              `Input Validation failed. Min: 1, Max: 10 ${key.toLowerCase()} players are allowed `,
              `Invalid number of players from team ${key.toLowerCase()} : ${value}`
            )
          );
      }
    }
  }

  next();
};

module.exports = validateAddTeam;
