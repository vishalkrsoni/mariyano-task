const { Schema, model } = require("mongoose");

const fantasyTeamSchema = new Schema(
  {
    teamName: { type: String, unique: true, required: true },
    players: [{ type: String, required: true }],
    captain: { type: String, required: true },
    viceCaptain: { type: String, required: true },
    battingPoints: { type: Number, default: 0 },
    bowlingPoints: { type: Number, default: 0 },
    totalPoints: { type: Number, default: 0 },
    playerPoints: { type: Object, default: {} },
  },
  { timestamps: true }
);

const FantasyTeam = model("FantasyTeam", fantasyTeamSchema);

module.exports = FantasyTeam;
