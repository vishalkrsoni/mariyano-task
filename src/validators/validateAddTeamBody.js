const Joi = require("joi");

// Validating request body
const validateAddTeamBody = Joi.object({
  teamName: Joi.string().required().messages({
    "any.required": "Team name is required.",
    "string.empty": "Team name must not be empty.",
  }),
  players: Joi.array()
    .items(Joi.string())
    .min(11)
    .max(11)
    .unique()
    .required()
    .messages({
      "any.required": "Players list is missing.",
      "array.min": "Exactly 11 players must be selected.",
      "array.max": "Exactly 11 players must be selected.",
      "array.unique": "All Players must be unique.",
    }),
  captain: Joi.string().required().messages({
    "any.required": "Captain is required.",
    "string.empty": "Captain must not be empty.",
  }),
  viceCaptain: Joi.string().required().messages({
    "any.required": "Vice captain is required.",
    "string.empty": "Vice captain must not be empty.",
  }),
});

module.exports = { validateAddTeamBody };
