const { getPlayerTypeByPlayerName } = require("./filterPlayers");

const getBattingStats = (matchData, playersList, playerName) => {
  const playerStats = {
    runs: 0,
    fours: 0,
    sixes: 0,
    ballsPlayed: 0,
    dismissalDetails: [],
  };
  playerStats["playerType"] = getPlayerTypeByPlayerName(
    playersList,
    playerName
  );
  matchData.forEach((ball) => {
    if (ball.player_out === playerName) {
      // console.log(
      //   `${playerName} got out in over: ${ball.overs} at ball: ${ball.ballnumber}`
      // );
      playerStats.dismissalDetails.push({
        over: ball.overs,
        ball: ball.ballnumber,
        kind: ball.kind,
        fielders: ball.fielders_involved,
      });
    }
    if (ball.batter === playerName) {
      playerStats["ballsPlayed"]++;
      if (ball.batsman_run === 4) playerStats["fours"]++;
      if (ball.batsman_run === 6) playerStats["sixes"]++;

      playerStats["runs"] += ball.batsman_run;
    }
  });

  return playerStats;
};

const getBowlingStats = (matchData, playersList, playerName) => {
  const playerStats = {
    overs: 0,
    ballsDelivered: 0,
    maidenOvers: 0,
    wickets: 0,
    lbwOrBowled: 0,
    extras: {
      wides: 0,
      noBalls: 0,
      byes: 0,
      legByes: 0,
      penaltyRuns: 0,
    },
  };
  playerStats["playerType"] = getPlayerTypeByPlayerName(
    playersList,
    playerName
  );
  let consecutiveDotBalls = 0;

  matchData.forEach((ball) => {
    if (ball.bowler === playerName) {
      playerStats["ballsDelivered"]++;
      if (ball.isWicketDelivery == 1) playerStats["wickets"]++;

      if (ball.kind === "lbw" || ball.kind === "bowled")
        playerStats["lbwOrBowled"]++;

      if (ball.batsman_run === 0 && ball.extras_run === 0) {
        consecutiveDotBalls++;
        if (consecutiveDotBalls === 6) {
          playerStats.maidenOvers++;
          consecutiveDotBalls = 0;
        }
      } else {
        consecutiveDotBalls = 0;
      }

      // Update extras count
      switch (ball.extra_type) {
        case "wides":
          playerStats.extras.wides++;
          break;
        case "no ball":
          playerStats.extras.noBalls++;
          break;
        case "byes":
          playerStats.extras.byes++;
          break;
        case "leg byes":
          playerStats.extras.legByes++;
          break;
        case "penalty":
          playerStats.extras.penaltyRuns++;
          break;
        default:
          break;
      }
    }
  });

  const overs = Math.floor(playerStats["ballsDelivered"] / 6);
  const ballsRemaining = playerStats["ballsDelivered"] % 6;
  playerStats["overs"] = overs + ballsRemaining / 10;
  return playerStats;
};

const getFieldingStats = (matchData, playersList, playerName) => {
  const playerStats = {
    catches: 0,
    stumpings: 0,
    runOuts: 0,
  };
  playerStats["playerType"] = getPlayerTypeByPlayerName(
    playersList,
    playerName
  );

  matchData.forEach((ball) => {
    if (ball.kind === "caught and bowled" && ball.bowler === playerName)
      playerStats["catches"]++;

    if (ball.fielders_involved === playerName) {
      if (ball.kind == "caught") playerStats["catches"]++;
      if (ball.kind == "stumped") playerStats["stumpings"]++;
      if (ball.kind == "run_out") playerStats["runOuts"]++;
    }
  });
  console.log("field stat", { playerName, playerStats });
  return playerStats;
};

module.exports = { getBattingStats, getBowlingStats, getFieldingStats };
