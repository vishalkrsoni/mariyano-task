const getPlayerTypeByPlayerName = (playerList, playerName) => {
  const player = playerList.find((pl) => pl.Player === playerName);
  return player ? player.Role : null;
};

const isValidPlayer = (playerList, playerName) => {
  return playerList.some((pl) => pl.Player === playerName);
};

const getPlayerTeamByPlayerName = (playerList, playerName) => {
  const player = playerList.find((pl) => pl.Player === playerName);
  return player ? player.Team : null;
};

const filterPlayersByTeam = (playerList, teamName) => {
  return playerList.filter((pl) => pl.Team === teamName);
};

const filterPlayersByRole = (playerList, roleName) => {
  return playerList.filter((player) => player.Role === roleName);
};

module.exports = {
  getPlayerTypeByPlayerName,
  getPlayerTeamByPlayerName,
  filterPlayersByTeam,
  filterPlayersByRole,
  isValidPlayer,
};
