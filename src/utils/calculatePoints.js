const calculateBattingPoints = (playerStats, isCaptain, isViceCaptain) => {
  let bonus = 0;
  let points = playerStats.runs;

  if (playerStats.fours > 0) bonus += playerStats.fours * 1;

  if (playerStats.sixes > 0) bonus += playerStats.sixes * 2;

  if (playerStats.runs >= 30 && playerStats.runs < 50) bonus += 4;

  if (playerStats.runs >= 50 && playerStats.runs < 100) bonus += 8;

  if (playerStats.runs >= 100) bonus += 16;

  if (playerStats.runs === 0 && playerStats.playerType != "BOWLER") points -= 2;

  points += bonus;
  if (isCaptain) points = points * 2;
  if (isViceCaptain) points = points * 1.5;

  return points;
};

const calculateBowlingPoints = (playerStats, isCaptain, isViceCaptain) => {
  let bonus = 0;
  let points = playerStats.wickets * 25;

  if (playerStats.lbwOrBowled > 0) bonus += playerStats.lbwOrBowled * 8;

  if (playerStats.wickets === 3) bonus += 4;
  if (playerStats.wickets === 4) bonus += 8;
  if (playerStats.wickets === 5) bonus += 16;
  if (playerStats.maidenOvers > 0) bonus += playerStats.maidenOvers * 12;

  points += bonus;
  if (isCaptain) points = points * 2;
  if (isViceCaptain) points = points * 1.5;
  return points;
};

const calculateFieldingPoints = (playerStats, isCaptain, isViceCaptain) => {
  let bonus = 0;
  let points = playerStats.catches * 8;

console.log(playerStats)
  console.log(points,'initial')
  if (playerStats.catches > 3) bonus += (playerStats.catches % 3) * 4;

  if (playerStats.stumpings > 0) bonus += 12;
  if (playerStats.runOuts > 0) bonus += 6;

  points += bonus;
  if (isCaptain) points = points * 2;
  if (isViceCaptain) points = points * 1.5;
  console.log(points,'++++++++')
  return points;
};
module.exports = {
  calculateBattingPoints,
  calculateBowlingPoints,
  calculateFieldingPoints,
};
