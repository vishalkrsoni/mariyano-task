const mongoose = require("mongoose");

const retryDelay = 5 * 1000;

async function mongoConnect(DB_NAME, MONGO_URL) {
  let retries = 0;
  try {
    mongoose.set("strictQuery", false);
    await mongoose.connect(MONGO_URL, {
      dbName: DB_NAME,
      maxPoolSize: 150,
      connectTimeoutMS: retryDelay,
    });
    console.log(`Connected to Mongo_DB`,DB_NAME);

    retries = 0;
  } catch (err) {
    retries++;
    console.error(
      `Error Connecting to DB. Tried ${retries} times: `,
      err.message
    );

    const waitTime = retryDelay * Math.pow(2, retries - 1);

    console.log(`Retrying connection in ${waitTime / 1000} seconds...`);

    await new Promise((resolve) => setTimeout(resolve, waitTime));

    // Recursively retry connection
    await mongoConnect(DB_NAME, MONGO_URL);
  }
}

module.exports = { mongoConnect };
