const { HttpStatusCode } = require("axios");

class APIResponse {
  constructor(statusCode, success, message, data = null, error = null) {
    this.statusCode = statusCode;
    this.success = success;
    this.message = message;
    this.data = data;
    this.error = error;
  }

  static success = (message = "Success", data) =>
    new APIResponse(HttpStatusCode.Ok, true, message, data, null);

  static created = (message = "Resource created", data) =>
    new APIResponse(HttpStatusCode.Created, true, message, data);
  
  static badRequest = (message = "Bad request", error) =>
    new APIResponse(HttpStatusCode.BadRequest, false, message, null, error);

  static unauthorized = (message = "Unauthorized request", error) =>
    new APIResponse(HttpStatusCode.Unauthorized, false, message, null, error);

  static forbidden = (message = "Forbidden", error) =>
    new APIResponse(HttpStatusCode.Forbidden, false, message, null, error);

  static notFound = (message = "Not found", error) =>
    new APIResponse(HttpStatusCode.NotFound, false, message, null, error);

  static conflict = (message = "Conflict occurred", error) =>
    new APIResponse(HttpStatusCode.Conflict, false, message, null, error);

  static internalServerError = (message = "Internal server error", error) =>
    new APIResponse(
      HttpStatusCode.InternalServerError,
      false,
      message,
      null,
      error
    );
  static methodNotAllowed = (message = "Method not allowed", error) =>
    new APIResponse(
      HttpStatusCode.MethodNotAllowed,
      false,
      message,
      null,
      error
    );
}

module.exports = APIResponse;
