const validateAddTeam = require("../middlewares/validateAddTeam");
const { Router } = require("express");
const { MatchController } = require("../controllers/matchController");
const { MatchService } = require("../services/matchService");

const matchRouter = Router();

const matchservice = new MatchService();
const matchController = new MatchController(matchservice);

// required routes
matchRouter.post(
  "/add-team",
  validateAddTeam,
  matchController.addNewFantasyTeam.bind(matchController)
);

matchRouter.get(
  "/process-result",
  matchController.calculateResultForAllTeams.bind(matchController)
);

matchRouter.get(
  "/team-result",
  matchController.getResultForAllTeams.bind(matchController)
);

// helper routes -- extras
matchRouter.get(
  "/players/csk",
  matchController.getPlayersListOfCSK.bind(matchController)
);

matchRouter.get(
  "/players/rr",
  matchController.getPlayersListOfRR.bind(matchController)
);

matchRouter.get(
  "/get-all-teams",
  matchController.getAllTeamsInfo.bind(matchController)
);

module.exports = { matchRouter };
