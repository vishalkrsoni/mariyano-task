const { HttpStatusCode } = require("axios");

class MatchController {
  constructor(service) {
    this.service = service;
  }

  async addNewFantasyTeam(req, res) {
    try {
      const response = await this.service.addNewFantasyTeam(req.body);

      return res.status(response.statusCode).send(response);
    } catch (error) {
      return res.status(HttpStatusCode.InternalServerError).send(error);
    }
  }

  async calculateResultForAllTeams(req, res) {
    try {
      const response = await this.service.calculateResultForAllTeams();

      return res.status(response.statusCode).send(response);
    } catch (error) {
      return res.status(HttpStatusCode.InternalServerError).send(error);
    }
  }

  async getResultForAllTeams(req, res) {
    try {
      const response = await this.service.getResultForAllTeams();

      return res.status(response.statusCode).send(response);
    } catch (error) {
      return res.status(HttpStatusCode.InternalServerError).send(error);
    }
  }

  async getAllTeamsInfo(req, res) {
    try {
      const response = await this.service.getAllTeamsInfo();

      return res.status(response.statusCode).send(response);
    } catch (error) {
      return res.status(HttpStatusCode.InternalServerError).send(error);
    }
  }

  getPlayersListOfRR(req, res) {
    try {
      const response = this.service.getPlayersListOfRR();

      return res.status(response.statusCode).json(response);
    } catch (error) {
      return res.status(HttpStatusCode.InternalServerError).send(error);
    }
  }

  getPlayersListOfCSK(req, res) {
    try {
      const response = this.service.getPlayersListOfCSK();

      return res.status(response.statusCode).send(response);
    } catch (error) {
      return res.status(HttpStatusCode.InternalServerError).send(error);
    }
  }
}

module.exports = { MatchController };
