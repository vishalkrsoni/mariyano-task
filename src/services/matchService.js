const FantasyTeam = require("../models/fantasyTeam");
const APIResponse = require("../utils/APIResponse");
const playersList = require("../../data/players.json");
const matchData = require("../../data/match.json");
const {
  getBattingStats,
  getBowlingStats,
  getFieldingStats,
} = require("../utils/playerStats");
const {
  filterPlayersByRole,
  filterPlayersByTeam,
} = require("../utils/filterPlayers");

const {
  calculateBattingPoints,
  calculateBowlingPoints,
  calculateFieldingPoints,
} = require("../utils/calculatePoints");

class MatchService {
  constructor() {}

  async addNewFantasyTeam(body) {
    try {
      const { teamName, players, captain, viceCaptain } = body;

      const existingTeam = await FantasyTeam.findOne({ teamName });
      if (existingTeam) {
        return APIResponse.badRequest("Team name already exists");
      }

      const newFantasyTeam = new FantasyTeam({
        teamName,
        players,
        captain,
        viceCaptain,
      });

      await newFantasyTeam.save();

      return APIResponse.created("Successfully added a new team");
    } catch (error) {
      console.error("Error in add new team:", error);
      return APIResponse.internalServerError("Internal Server Error", error);
    }
  }

  async calculateResultForAllTeams() {
    try {
      const teamsInfo = await FantasyTeam.find({}, { _id: 0 });

      for (const team of teamsInfo) {
        let teamBattingPoints = 0;
        let teamBowlingPoints = 0;
        const playerPoints = {};

        for (const player of team.players) {
          const battingStats = getBattingStats(matchData, playersList, player);
          const bowlingStats = getBowlingStats(matchData, playersList, player);
          const fieldingStats = getFieldingStats(
            matchData,
            playersList,
            player
          );

          const battingPoints = calculateBattingPoints(
            battingStats,
            player === team.captain,
            player === team.viceCaptain
          );
          const bowlingPoints = calculateBowlingPoints(
            bowlingStats,
            player === team.captain,
            player === team.viceCaptain
          );

          const fieldingPoints = calculateFieldingPoints(
            fieldingStats,
            player === team.captain,
            player === team.viceCaptain
          );

          playerPoints[player] = {
            battingPoints,
            bowlingPoints,
            fieldingPoints,
            totalPoints: battingPoints + bowlingPoints + fieldingPoints,
          };

          teamBattingPoints += battingPoints;
          teamBowlingPoints += bowlingPoints;
        }

        const totalPoints = teamBattingPoints + teamBowlingPoints;
        team.battingPoints = teamBattingPoints;
        team.bowlingPoints = teamBowlingPoints;
        team.totalPoints = totalPoints;
        team.playerPoints = playerPoints;

        await FantasyTeam.updateOne({ teamName: team.teamName }, team);
      }

      console.log("Team Scores Update success ");
      return APIResponse.success("Successfully calculated and updated result");
    } catch (error) {
      console.log("Error in calculateResultForAllTeams:", error);
      return APIResponse.internalServerError("Internal Server Error", error);
    }
  }

  async getResultForAllTeams() {
    try {
      const topTeams = await FantasyTeam.find(
        {},
        { _id: 0, teamName: 1, totalPoints: 1, playerPoints: 1 }
      ).sort({
        totalPoints: -1,
      });
      const maxPoints = topTeams.length > 0 ? topTeams[0].totalPoints : 0;

      const winningTeams = topTeams.filter(
        (team) => team.totalPoints === maxPoints
      );

      const winningTeamInfo = winningTeams.map((team) => ({
        teamName: team.teamName,
        totalPoints: team.totalPoints,
      }));

      return APIResponse.success(
        "Successfully found fantasy result for all teams",
        { winningTeam: winningTeamInfo, TeamRanking: topTeams }
      );
    } catch (error) {
      console.error("Error in getResultForAllTeams:", error);
      return APIResponse.internalServerError("Internal Server Error", error);
    }
  }

  // additional helper routes
  async getAllTeamsInfo() {
    try {
      const teamsInfo = await FantasyTeam.find(
        {},
        { _id: 0, teamName: 1, captain: 1, viceCaptain: 1, players: 1 }
      );

      return APIResponse.success("Successfully found all team info", teamsInfo);
    } catch (error) {
      console.error("Error in getResultForAllTeams:", error);
      return APIResponse.internalServerError("Internal Server Error", error);
    }
  }

  getPlayersInfoByTeam(teamName) {
    try {
      const players = filterPlayersByTeam(playersList, teamName);

      const batters = filterPlayersByRole(players, "BATTER");
      const bowlers = filterPlayersByRole(players, "BOWLER");
      const keepers = filterPlayersByRole(players, "WICKETKEEPER");
      const allRounders = filterPlayersByRole(players, "ALL-ROUNDER");

      return APIResponse.success(
        `Successfully got data of ${teamName} players`,
        {
          batters,
          bowlers,
          keepers,
          allRounders,
        }
      );
    } catch (error) {
      console.error("Error fetching player data:", error);
      throw error;
    }
  }

  getPlayersListOfRR() {
    try {
      return this.getPlayersInfoByTeam("Rajasthan Royals");
    } catch (error) {
      console.log("Error in getPlayersListOfRR:", error);
      return APIResponse.internalServerError("Internal Server Error", error);
    }
  }

  getPlayersListOfCSK() {
    try {
      return this.getPlayersInfoByTeam("Chennai Super Kings");
    } catch (error) {
      console.log("Error in getPlayersListOfCSK:", error);
      return APIResponse.internalServerError("Internal Server Error", error);
    }
  }
}

module.exports = { MatchService };
